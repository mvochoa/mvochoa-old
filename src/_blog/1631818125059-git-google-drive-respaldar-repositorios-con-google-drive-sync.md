---
slug: 'git-google-drive-respaldar-repositorios-con-google-drive-sync'
title: 'Git + Google Drive - respaldar repositorios con Google Drive Sync'
summary: 'Respaldar un repositorio Git dentro de Google Drive automáticamente al hacer un push.'
date: '2019-01-16'
tags: 'Git, Google Drive'
image: '/images/blog/1631818125059/thumbnail.png'
---

Hola, siempre he tenido la duda de que hacer, si falla Github o Gitlab y no pueda descargar el repositorio. Bueno encontré un forma de respaldar los repositorios en Google Drive muy fácilmente.

## Requisitos

1. Es necesario tener instalado **Google Drive Sync** y **Git** para poder sincronizar desde nuestra equipo.
    - _Recomendación_: A la carpeta que sincronicen con **Google Drive Sync** que no tenga espacios. Ejemplos: `GDrive`, `GoogleDrive`, `GoogleDriveSync`, etc.
2. Tener una cuenta, ya sea en Gitlab, Github o en algún servicio de repositorios remotos.

## Pasos a seguir

1. **Los siguientes pasos se pueden omitir, si ya tienes clonado el repositorio.**
    - Crear el repositorio en **Gitlab**, **Github** o en algún servicio de repositorios remotos.
    - Clonar el repositorio, en mi caso va ser en documentos. Ejemplo: `git clone git@gitlab.com:mvochoa/test.git ~/Documents/test`
2. Entrar a la carpeta del repositorio clonado. Ejemplo: `cd ~/Documents/test/`
3. Crear la carpeta y copiar los archivos para el repositorio dentro de la carpeta sincronizada con **Google Drive Sync**, en mi caso se llama _GoogleDrive_. Ejemplo: `git clone --bare . ~/GoogleDrive/mvochoa/test.git`
    - Esto va a crear la carpeta y va copiar los archivos que usualmente están en la capeta `.git` dentro de los repositorios.
4. Por ultimo, solo toca agregar a la rama origin el repositorio de Google Drive que creamos, con los siguientes comandos:

```sh
$ git remote set-url --add origin ~/GoogleDrive/mvochoa/test.git
$ git remote -v

origin    git@gitlab.com:mvochoa/test.git (fetch)
origin    git@gitlab.com:mvochoa/test.git (push)
origin    /Users/mvochoa/GoogleDrive/mvochoa/test.git (push)
```

Como puedes ver se agrega la url de la carpeta de Google Drive como si fuera un repositorio, pero solo para _push_. Esto quiere decir que cuando se ejecute el comando: `git push origin <RAMA>` se van a subir los cambios a nuestro repositorio remoto y a nuestro repositorio en Google Drive, ya que Google Drive Sync lo está sincronizando.

## Pruebas

Vamos a probar si se suben cambios a los dos repositorios.

```sh
$ echo "Hola Mundo" >> README.md
$ git add -A
$ git commit -m "Add README.md"
$ git push origin master

Counting objects: 3, done.
Writing objects: 100% (3/3), 237 bytes | 237.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To gitlab.com:mvochoa/test.git
 * [new branch]      master -> master
Counting objects: 3, done.
Writing objects: 100% (3/3), 237 bytes | 237.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To /Users/mvochoa/GoogleDrive/mvochoa/test.git
 * [new branch]      master -> master
```

Ahora voy a clonar el repositorio de Google Drive para ver si tiene los archivos.

```sh
$ git clone ~/GoogleDrive/mvochoa/test.git ~/Documents/gdriveTest
$ cd ~/Documents/gdriveTest
$ ls
README.md

$ cat README.md
Hola Mundo

$ git log
commit 2b29a339410a070d2df17775fe25313ad9a6bfdd (HEAD -> master, origin/master, origin/HEAD)
Author: Mario Valentin Ochoa Mota <mvochoaa@gmail.com>
Date:   Tue Jan 15 15:03:47 2019 -0600

    Add README.md
```

Aquí comprobamos que si se están subiendo los cambios.

Bueno esto es todo, espero que se de ayuda.
