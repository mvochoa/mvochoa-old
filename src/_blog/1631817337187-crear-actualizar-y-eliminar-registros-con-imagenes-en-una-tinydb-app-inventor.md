---
slug: 'crear-actualizar-y-eliminar-registros-con-imagenes-en-una-tinydb-app-inventor'
title: 'Crear, actualizar y eliminar registros con imágenes en una TinyDB | App Inventor'
summary: 'Como crear una aplicación con app inventor para crear, actualizar y eliminar registros de una TinyDB con imágenes usando una ListView.'
date: '2018-07-31'
tags: 'App Inventor, TinyDB'
image: '/images/blog/1631817337187/thumbnail.png'
---

![App CRUD con imágenes TinyDB](/images/blog/1631817337187/1531499070.gif) La aplicación que vamos a crear va ser un registro de películas con su poster, los únicos datos que vamos almacenar va ser el `Titulo`, `Año` y la `Imagen`.

## Requisitos para el desarrollo

- Es necesario una extensión para convertir la imagen a BASE64 y poder guardarla en la tinyDB. La puedes descargar en esta publicación [Crear una extension para convertir imágenes a base64 y viceversa](/blog/como-crear-una-extension-para-convertir-imagenes-en-base64-y-viceversa-app-inventor). También explico como desarrolle la extensión por si quieres hacerle cambios.

Creamos el proyecto en app inventor en mi caso con el nombre `CRUDListView`, las propiedades de la pantalla que se cambiaron son las siguientes:

- AlignHorizontal: center
- Theme: Device Default
- TitleVisible: false *Esta misma en propiedad se cambio en todas pantallas*

## La primera pantalla

En la pantalla principal es donde se encuentra la lista de las películas, los componente que la constituyen son los siguientes:

- **HorizontalArrangement**
    - AlignVertical: center
    - Width: 90%
    - **Componentes:**
        - **Label**
            - Width: Fill Parent
            - FontSize: 24
            - Text: Películas
        - **Button**
            - Name: BtnNew
            - FontBold: true
            - Text: NUEVO
- **ListView**
    - Name: ListViewMovies
    - Width: 90%
    - ScalePictureToFit: true
- **TinyDB**
- **Notifier**

![Interfaz de la app del crud de la tinyDB Pantalla 1](/images/blog/1631817337187/1531501230.png)

## Bloques de la primera pantalla

Para la estructura de la tinyDB va ser de una lista que contiene listas con los datos en el siguiente orden: (**Titulo** ~ Cadena, **Año** ~ Cadena, **Imagen** ~ Base64).

![Proceso initialize de la primera pantalla](/images/blog/1631817337187/1531504806w300.png) Empezamos por crear un proceso que llamamos `Start`. Va ser necesario declarar dos variables globales `Data` y `Titles`. Para la variable `Data` le asignamos el valor que tiene guardado la tinyDB en el tag *database*. A la variable Titles le asignamos una lista vaciá y luego iteramos la lista obtenida anteriormente y de cada elemento de la lista seleccionamos el primero que es el titulo y lo agregamos a la lista de `Titles`. Por ultimo la lista de Titles se asignamos a la propiedad *Elements* del componente **ListView**.

![Event Initialize and CloseScreen](/images/blog/1631817337187/1531504928w300.png) Llamamos al proceso `Start` en el evento *Initialize* y *OtherScreenClosed* para que cuando inicie la aplicación o se cierre otro screen se actualicen los datos que se muestra en la ListView.

![Event AfterPiking de ListView](/images/blog/1631817337187/1531501906w300.png) En el evento *AfterPicking* del componente **ListView** que se ejecuta cuando seleccionamos un elemento de la lista una vez que se active el evento ejecutamos el método *ShowChooseDialog* del *Notifier*, para que muestre un cuadro de dialogo para que el usuario elija si quiere *Editar* o *Eliminar* el elemento que escogió.

El método *ShowChooseDialog* ejecuta el evento *AfterChoosing*, para saber que acción se hizo enviá una variable que contiene lo que esta escrito en el botón que lo invoco, mediante una condicional vamos a definir que se va hacer en cada uno de los casos.

![Event AfterChoosing de Notifier](/images/blog/1631817337187/1531503431.png)

El primer caso es cuando se click en botón `Editar`. Seleccionamos la lista que esta en `Data` en la posición del elemento seleccionado de la ListView. Recuerda la variable `Data` es una lista que contiene listas.

A la lista que extrajimos que contiene todos los datos Titulo, Año y la Imagen le agregamos otro valor que va ser su posición actual en la variable `Data`. Una vez agregado el valor abrimos la pantalla llamada `Form` y le enviamos la lista. Mas adelante se explica que se hace en la pantalla `Form`.

![Event AfterChoosing de Notifier](/images/blog/1631817337187/1531503922w300.png) En el siguiente caso es cuando se de click en botón `Eliminar` del notifier vamos a volver a ejecutar el método *ShowChooseDialog* pero con el mensaje para confirmar si desea borrar el elemento. Esto va a provocar que se vuelva a llamar el evento *AfterPicking* lo que provoca un tercer caso.

![Event AfterChoosing de Notifier](/images/blog/1631817337187/1531504119w600.png) El tercer caso es que se de click en `Si` lo que hacemos en este caso es borrar el elemento de la lista `Data` y `Titles` volvemos asignar la lista de Titles en la propiedad *Elements* del componente **ListView**. Remplazamos el valor de la tinyDB en el tag *databases* y mandamos abrir un cuadro de dialogo para notificar al usuario que ya se elimino el registro.

Cualquier otro caso como `Cancel` o `No` simplemente son ignorados en el evento *AfterChoosing*.

![Alt image](/images/blog/1631817337187/1531504537w300.png) Ya por ultimo en la pantalla principal en el botón de Nuevo abrimos la pantalla `Form` y como valor le mandamos *false*.

<p></p>
## La pantalla del formulario (`Form`)

En esta pantalla se va Editar o Crear nuevos registros de películas para eso van a ser necesario los siguientes componentes:

- **Image**
    - ScalePictureToFit: true
    - Width: 90%
- **HorizontalArrangement**
    - Width: 90%
    - **Componentes:**
        - **ImagePicker**
            - Name: BtnImagePicker
            - FontBold: true
            - Width: Fill Parent
            - Text: SELECCIONAR IMAGEN
        - **Button**
            - Name: BtnCleanImage
            - FontBold: true
            - Text: BORRAR
- **TextBox**
    - Name: TxbTitle
    - Width: 90%
    - Hint: Titulo
- **HorizontalArrangement**
    - Width: 90%
    - **Componentes:**
        - **TextBox**
            - Name: TxbYear
            - Width: Fill Parent
            - Hint: Año
            - NumbersOnly: true
        - **Button**
            - Name: BtnAction
            - FontBold: true
            - Text: GUARDAR
- **VerticalArrangement**
    - Height: Fill Parent
    - Width: 1px
- **Button**
    - Name: BtnBack
    - FontBold: true
    - Text: VOLVER
- **Base64** *Extensión mencionada en los requisitos*
- **TinyDB**
- **Notifier**

![Screen Form](/images/blog/1631817337187/1532629098.png)

## Bloques para el formulario

Hay que recordar que el formulario va a funcionar para registrar nuevas películas y editar. Para validar la acción que se va hacer lo haremos en el evento *Initialize* del Screen.

![Alt image](/images/blog/1631817337187/1532629431w300.png) Declaramos la variable `Data` para almacenar la lista de películas y poder editar o agregar nuevos registros. Recuperamos los registros de la tinyDB con el tag *database*.

Para abrir este pantalla siempre se enviá un dato que puede ser una Lista o Booleano, ya sabes que en caso de que se una lista es para editar información si es Booleano es para hacer un nuevo registro.

En caso de que se una lista haremos lo siguiente: En el componente **TxbTitle** asignamos en su atributo *Text* el valor de titulo de la lista que se envió desde el screen anterior. Seria lo mismo para el Año, en caso para la Imagen hay que validar que exista registro en caso de que si, usamos el método *Base64ToImage* de la extensión **Base64** en caso de que no solo asignamos un cadena vaciá. Por ultimo en el **BtnAction** le cambiamos el atributo *Text* a ACTUALIZAR.

![Alt image](/images/blog/1631817337187/1532630029.png)

![Alt image](/images/blog/1631817337187/1532630114w300.png) Para el caso que se envié un Booleano solo vaciamos los campos y  el **BtnAction** le cambiamos el atributo *Text* a GUARDAR.

![Alt image](/images/blog/1631817337187/1532630332w300.png) Para el botón **BtnImagePicker** en su evento *AfterPicking* solo asignamos la imagen escogida al componente **Image** y para el botón **BtnCleanImage** le asignamos una cadena vacía al componente **Image**.

![Alt image](/images/blog/1631817337187/1532630404w300.png) Creamos el procedimiento `Close` que se va encargar de ocultar el teclado y cerrar el screen. Lo vamos a ejecutar en **BtnBack** en su evento *Click*.

![Alt image](/images/blog/1631817337187/1532630642.png)

Ahora en el evento *Click* del **BtnAction** vamos a crear un variable local llamada `item` para almacenar la lista que se va actualizar o editar. Igualmente validamos si hay una imagen seleccionada para convertirla a Base64 con el método *ImageToBase64* de la extension.

Mediante una condicional vamos a validar si en el **BtnAction** es su atributo *Text* esta escrito ACTUALIZAR ó GUARDAR.

![Alt image](/images/blog/1631817337187/1532630995w300.png) En caso de que se ACTUALIZAR vamos a remplazar la lista en `Data` con el valor de `item` en el index que se envió al screen y notificamos al usuario de que se realizo la acción. En caso de que se GUARDAR agregamos `item` a lista `Data` e igualmente notificamos al usuario.

![Alt image](/images/blog/1631817337187/1532631105w300.png) Por ultimo actualizamos la información en la tinyDB y llamamos al procedimiento `Close`.

Bueno eso seria todo te comparto los enlaces de los archivos:

- Proyecto de la app: [CRUDListView.aia](http://bit.ly/2AjsXlM)
- Apk de la app: [CRUDListView.apk](http://bit.ly/2v7717F)

Espero que te haya sido de ayuda. No olvides si ha sido util para ti seria de mucha ayuda si compartes este material con tus amigos.