---
slug: 'configuracion-del-servicio-de-mailgun-para-envio-y-recepcion-de-correos'
title: 'Configuración del servicio de Mailgun para envió y recepción de correos'
summary: 'Configuración del servicio mailgun para poder enviar correos y recibir correos de una cuenta personalizada como por ejemplo: nombre@dominio.com'
date: '2018-06-30'
tags: 'Mailgun, SMTP'
image: '/images/blog/1631816019591/thumbnail.png'
---

Vamos a configurar una cuenta de mailgun para poder enviar correos y recibir correos de una cuenta como por ejemplo: nombre@dominio.com.

## Requisitos para poder configurar la cuenta

-   Es necesario tener una cuenta en: [https://mailgun.com/](https://mailgun.com/) ya verificada es decir con la validación del correo.
-   Una cuenta en [http://digitalocean.com/](https://m.do.co/c/c0bd5d36b74c) para la configurar el dominio. Te recomiendo usar el enlace asi te van a regalar \$10 dolares para usar en tu cuenta.
-   Tener un dominio.

Una vez que tengas cumplidos los requisitos, hay que añadir el dominio a la cuenta de mailgun. **NO** uses un subdominio nunca he logrado que funcione.

![Botón de añadir dominio mailgun](/images/blog/1631816019591/1531257978.png)
![Formulario para añadir dominio a mailgun](/images/blog/1631816019591/1531258000.png)

Ahora en _DigitalOcean_ en el apartado de Networking añadimos el dominio. Para que funcione se deben cambiar los **NameServers** de tu dominio en donde lo compraste por los de DigitalOcean.

-   ns1.digitalocean.com
-   ns2.digitalocean.com
-   ns3.digitalocean.com

![Formulario para añadir dominio en DigitalOcean](/images/blog/1631816019591/1531258023.png)

Ahora registramos todos los _DNS Records_ la información la vamos a copiar de la pagina de mailgun a la de DigitalOcean.

### TXT

![TXT Mailgun](/images/blog/1631816019591/1531258045.png)
![TXT DigitalOcean](/images/blog/1631816019591/1531258072.png)
![TXT 2 DigitalOcean](/images/blog/1631816019591/1531258097.png)

### MX

![MX Mailgun](/images/blog/1631816019591/1531258122.png)
![MX DigitalOcean](/images/blog/1631816019591/1531258142.png)
![MX 2 DigitalOcean](/images/blog/1631816019591/1531258200.png)

### CNAME

![CNAME Mailgun](/images/blog/1631816019591/1531258226.png)
![CNAME DigitalOcean](/images/blog/1631816019591/1531258262.png)

Listo eso seria la configuración para DigitalOcean, ya solo queda esperar a que se validen los DNS Records pueden tardar de 24 a 48 horas.

## Creamos las credenciales SMTP

Hay que crear nuestra cuenta nombre@dominio.com se hace en el apartado de dominios de mailgun y creamos una nueva credencial, en mi caso yo registre el correo mario@mvochoa.com.

![Manage SMTP credentials](/images/blog/1631816019591/1531258294.png)
![Nueva credencial SMTP](/images/blog/1631816019591/1531258317.png)

Una vez creado la nueva credencial, tenemos que registrar un ruta para poder redireccionar todos los correos que envíen a mario@mvochoa.com lleguen al correo de Gmail mvochoaa@gmail.com.

![Crear ruta mailgun](/images/blog/1631816019591/1531258350.png)
![registrar ruta mailgun](/images/blog/1631816019591/1531258369.png)

Primero vamos a probar y vamos a enviar un correo a mario@mvochoa.com y debemos recibirlo en la cuenta de Gmail.

Enviamos el correo de outlook
![Enviar email](/images/blog/1631816019591/1531258392.png)

y lo recibimos en la cuenta de Gmail como vez es para **mario@mvochoa.com**.

![Email recibido](/images/blog/1631816019591/1531258855.png)

También podemos enviar correos de la cuenta de mario@mvochoa.com, primero vamos a usar el comando `curl` con el **Api Key** que se encuentra en la información del dominio en mailgun, para enviar un correo a la misma cuenta de outlook como si le enviáramos una "respuesta".

```
$ curl -s --user 'api:key-5f34hsf74lsd7bd48b71bc3388a' \
    https://api.mailgun.net/v3/mvochoa.com/messages \
    -F from='Mario Valentin <mario@mvochoa.com>' \
    -F to=vale_ochoa29@hotmail.com \
    -F subject='Respuesta Curl' \
    -F text='Esto es una prueba de la respuesta'
```

debemos recibir como respuesta del comando:

```
{
  "id": "<20180630021806.1.3B8671DC0426B708@mvochoa.com>",
  "message": "Queued. Thank you."
}
```

y con eso debemos recibir el correo en la cuenta de outlook:

![Respuesta en outlook](/images/blog/1631816019591/1531259280.png)

Por ultimo vamos a enviar un correo usando las credenciales SMTP, para eso vamos usar un script en Golang (Go) para enviar el correo.

Las mismas credenciales SMTP se pueden usar en cualquier otro servicio o lenguaje.

```
package main

import (
	"log"
	"net/smtp"
)

const mime = "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"

func main() {
	msg := "To: vale_ochoa29@hotmail.com\r\nSubject: Respuesta SMTP\r\n" + mime + "\r\nEsto es una prueba de la respuesta"
	auth := smtp.PlainAuth(
		"",
		"mario@mvochoa.com",
		"LA CONTRASEÑA",
		"smtp.mailgun.org")
	err := smtp.SendMail(
		"smtp.mailgun.org:587",
		auth,
		"mario@mvochoa.com",
		[]string{"vale_ochoa29@hotmail.com"},
		[]byte(msg),
	)
	if err != nil {
		log.Fatal("No se pudo enviar el correo.")
	}

	log.Println("Listo se a enviado el correo.")
}
```

![Respuesta SMTP](/images/blog/1631816019591/1531259704.png)

Eso seria todo como puedes ver se pueden enviar y recibir correo electrónicos usando Mailgun.

Espero que te haya sido de ayuda. No olvides si ha sido util para ti seria de mucha ayuda si lo compartes con tus amigos.
