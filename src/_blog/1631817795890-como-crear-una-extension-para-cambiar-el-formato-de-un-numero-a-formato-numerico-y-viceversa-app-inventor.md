---
slug: 'como-crear-una-extension-para-cambiar-el-formato-de-un-numero-a-formato-numerico-y-viceversa-app-inventor'
title: 'Como crear una extensión para cambiar el formato de un número a formato numérico y viceversa | App Inventor'
summary: 'La intención de la extensión es poder convertir, un numero que esta en formato numérico "12345.67" a formato de moneda "$12,345.67" y que el símbolo de la moneda se pueda especificar "€, $, ƒ". Es algo muy sencillo pero en app inventor a veces es necesario hacer esta conversión en varios screens y hay que estar copiando procesos, lo cual es muy tedioso por eso usando la extensión se puede ahorrar trabajo.'
date: '2019-01-15'
tags: 'App Inventor, Moneda'
image: '/images/blog/1631817795890/thumbnail.png'
---

La intención de la extensión es poder convertir, un numero que esta en formato numérico "12345.67" a formato de moneda "$12,345.67" y que el símbolo de la moneda se pueda especificar "€, $, ƒ". Es algo muy sencillo pero en app inventor a veces es necesario hacer esta conversión en varios screens y hay que estar copiando procesos, lo cual es muy tedioso por eso usando la extensión se puede ahorrar trabajo.

> Si tu no tienes el interés de saber como se hace la extensión, puedes ver al final de la publicación los enlaces para descargarla y como se usa.

## Configuración del proyecto y archivos

Primero que todo va ser necesario clonar el repositorio de App Inventor:

```sh
$ cd ~/
$ git clone https://github.com/mit-cml/appinventor-sources.git
```

Una vez clonado el repositorio, vamos a crear la ruta de nuestro paquete y el archivo java. El nombre del paquete no es muy relevante pero yo siempre uso el nombre de mi dominio claramente en forma de paquete.

```sh
$ cd ~/appinventor-sources/appinventor/components/src
$ mkdir -p com/mvochoa/MoneyFormat
$ cd com/mvochoa/MoneyFormat
$ touch MoneyFormat.java
```

## Estructura de la clase MoneyFormat

Dentro del archivo `MoneyFormat.java` creamos la clase **MoneyFormat**, hay que agregar los datos de la extensión y especificar que es un componente externo y no un componente no visible.

Los datos que se especifican de la extensión son:

- **Version:** 1
- **Descripción:** Cambia el formato de numérico a formato de moneda y viceversa
- **Categoría:** EXTENSION
- **No Visible:** No va ser visible en un aspecto gráfico dentro de las apps.
- **Icono:** El icono de extensión por defecto.

```java
package com.mvochoa.MoneyFormat;

import com.google.appinventor.components.annotations.DesignerComponent;
import com.google.appinventor.components.common.ComponentCategory;
import com.google.appinventor.components.annotations.SimpleObject;
import com.google.appinventor.components.runtime.AndroidNonvisibleComponent;
import com.google.appinventor.components.runtime.ComponentContainer;

@DesignerComponent(version = 1,
description = "Cambia el formato de numérico a formato de moneda y viceversa",
category = ComponentCategory.EXTENSION,
nonVisible = true,
iconName = "images/extension.png")
@SimpleObject(external = true)
public class MoneyFormat extends AndroidNonvisibleComponent {

    public MoneyFormat(ComponentContainer container) {
        super(container.$form());
    }

}
```

## Propiedades del componente

La propiedad que se debe tener es el símbolo de la moneda, va ser un campo de texto donde se pueda colocar el símbolo, también los métodos para poder cambiar el símbolo desde los bloques.

Primero agregamos la variable global `coinSymbol` :

```java
// Imports ...

// Configuraciones de la extensiones ...
public class MoneyFormat extends AndroidNonvisibleComponent {

    private String coinSymbol = "$";

    // Constructor ...

}
```

Ahora el método para cambiar el símbolo, igual se debe agregar sus configuraciones del método para que se vean en app inventor:

Los datos que se especifican son los siguientes:

- **Descripción:** Especifica el símbolo de la moneda.
- Propiedades del diseño:
    - **Tipo de editor:** STRING
    - **Valor por defecto:** $

Para el método de retornar el símbolo, sus propiedades son:

- **Descripción:** Devuelve el símbolo de la moneda.
- **Categoría:** APPEARANCE

```java
// Imports anteriores ...
import com.google.appinventor.components.annotations.DesignerProperty;
import com.google.appinventor.components.common.PropertyTypeConstants;
import com.google.appinventor.components.annotations.SimpleProperty;
import com.google.appinventor.components.annotations.PropertyCategory;

// Configuraciones de la extensiones ...
public class MoneyFormat extends AndroidNonvisibleComponent {

    // Constructor y variables globales ...

    @DesignerProperty(editorType = PropertyTypeConstants.PROPERTY_TYPE_STRING, defaultValue = "$")
    @SimpleProperty(description = "Especifica el símbolo de la moneda.")
    public void CoinSymbol(String coinSymbol) {
        this.coinSymbol = coinSymbol;
    }

    @SimpleProperty(description = "Devuelve el símbolo de la moneda.", category = PropertyCategory.APPEARANCE)
    public String CoinSymbol() {
        return this.coinSymbol;
    }

}
```

## Métodos para hacer las conversiones

Son dos métodos uno para convertir de formato numero a formato moneda y otro viceversa.

El método para convertir de formato numérico a formato moneda `NumberToMoney(String number)`. La lógica es muy sencilla se valida que sea un número y se le da el formato con solo 2 decimales, después separamos la parte entera del número la cual se itera de derecha a izquierda y cada tres dígitos agregamos una "," y retornamos el número concatenado con el símbolo de la moneda.

Las propiedades del método son:

- **Descripción:** Convierte un número en formato numérico "12345.34" a formato de moneda "$12,345.34".

Para el segundo método para convertir de formato moneda a formato numérico `MoneyToNumber(String numberMoney)`. Lo que hace el método solo se borra el símbolo de la moneda y las comas del número.

Las propiedades del segundo método son:

- **Descripción:** Convierte un número en formato de moneda "$12,345.34" a formato numérico "12345.34".

```java
// Imports anteriores ...
import com.google.appinventor.components.annotations.SimpleFunction;

// Configuraciones de la extensiones ...
public class MoneyFormat extends AndroidNonvisibleComponent {

    // Constructor, variables globales y métodos anteriores ...

    @SimpleFunction(description = "Convierte un numero en formato numérico \"12345.34\"" +
     "a formato de moneda \"$12,345.34\".")
    public String NumberToMoney(String number) {
        try {
            String num = String.format("%.2f", Double.parseDouble(number)); // Se comprueba que sea un numero
            String[] numArray = num.split("\\.");
            for (int i = numArray[0].length() - 3; i > 0; i -= 3) {
                numArray[0] = String.format("%s,%s", numArray[0].substring(0, i),
                        numArray[0].substring(i, numArray[0].length()));
            }

            return String.format("%s%s.%s", coinSymbol, numArray[0], numArray[1]);
        } catch (Exception e) {
            return number;
        }
    }

    @SimpleFunction(description = "Convierte un numero en formato de moneda \"$12,345.34\"" +
    " a formato numérico \"12345.34\".")
    public String MoneyToNumber(String numberMoney) {
        return numberMoney.replaceAll("[" + coinSymbol + ",]", "");
    }

}
```

## Compilemos la extensión

Ya solo queda compilar la extensión para poder usarla en app inventor. Hay que ejecutar los siguientes comandos:

```sh
$ cd ~/appinventor-sources/appinventor
$ ant clean
$ ant extensions
```

Listo con eso tenemos nuestro archivo `.aix` en la ruta: `~/appinventor-sources/appinventor/components/build/extensions/com.mvochoa.MoneyFormat.aix`

El archivo `com.mvochoa.MoneyFormat.aix` es el que tenemos que importar en app inventor.

## Vamos a probar la extensión

Entramos a [http://ai2.appinventor.mit.edu/](http://ai2.appinventor.mit.edu/) y creamos un nuevo proyecto.

![Crear proyecto](/images/blog/1631817795890/1546221324w300.png)

Una vez abierto el proyecto importamos la extensión que es el archivo `com.mvochoa.MoneyFormat.aix`.

![Importar extensión](/images/blog/1631817795890/1531257507w200.png)

Ahora creamos un interfaz para probar la extensión con los siguientes componentes:

- **TextBox**
    - Name: TextBox1
    - Width: 90%
    - Height: Automatic
    - Hint: Escribe aquí el numero
    - NumbersOnly: True
- **HorizontalArrangement**
    - Name: HorizontalArrangement1
    - Width: 90%
    - **Componentes:**
        - **Button**
            - Name: BtnMoneyFormat
            - Width: Fill Parent
            - Text: Formato Moneda
        - **Button**
            - Name: BtnMoneyNumber
            - Width: Fill Parent
            - Text: Formato Numérico
- **HorizontalArrangement**
    - Name: HorizontalArrangement2
    - Width: 90%
    - **Componentes:**
        - **Button**
            - Name: BtnEuros
            - Width: Fill Parent
            - Text: Euros
        - **Button**
            - Name: BtnPesos
            - Width: Fill Parent
            - Text: Pesos
- **MoneyFormat** | La extensión que acabamos de hacer.

![Interfaz de la app para probar la extensión de Base64](/images/blog/1631817795890/1546222234.png)

En los bloques solo van ser los eventos click de los botones:

![Button Money Format](/images/blog/1631817795890/1546223359.png)

En el botón `BtnMoneyFormat` va convertir el numero a formato moneda usando el método **NumberToMoney**.

![Button Money Number](/images/blog/1631817795890/1546223618.png)

En el botón `BtnMoneyNumber` va convertir el numero a formato numérico usando el método **MoneyToNumber**.

![Button Euros](/images/blog/1631817795890/1546223694w300.png) En el botón `BtnEuros`  asigna el símbolo *€* a la propiedad de símbolo de moneda.

![Button Pesos](/images/blog/1631817795890/1546223886w300.png) En el botón `BtnPesos`  asigna el símbolo *$* a la propiedad de símbolo de moneda.

## Probamos la Aplicación

![Gif del uso de la app para probar extensión MoneyFormat](/images/blog/1631817795890/1546264028w300.gif) Ya solo queda probar la aplicación este fue el resultado como puedes ver en le TextBox se ven las conversiones.

Enlaces de los archivos:

- Código fuente: [MoneyFormat.java](http://bit.ly/2EVGHnM)
- Extensión: [com.mvochoa.MoneyFormat.aix](http://bit.ly/2EZv7sq)
- Proyecto de la app: [UsoExtMoneyFormat.aia](http://bit.ly/2Tnhuqj)
- Apk de la app: [UsoExtMoneyFormat.apk](http://bit.ly/2EZ4eET)

Bueno eso seria todo, espero que sea de ayuda.