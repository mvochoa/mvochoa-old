export function sortByKey(key, asc = 1) {
  return (a, b) => {
    if (a[key] < b[key]) {
      return -1 * asc
    }
    if (a[key] > b[key]) {
      return 1 * asc
    }
    return 0
  }
}
