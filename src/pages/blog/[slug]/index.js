import moment from 'moment'

import Layout from '@/components/Layout'
import Markdown from '@/components/Markdown'
import SEO from '@/components/SEO'
import Adsense from '@/components/Adsense'

import { getAllSlugPages, getPageContentBySlug } from '@/lib/markdown'

export async function getStaticProps({ params }) {
  const { slug } = params
  const page = getPageContentBySlug(slug, '_blog', [
    'title',
    'date',
    'tags',
    'summary',
    'image',
    'content',
  ])
  return {
    props: {
      meta: page,
      slug: slug,
      summary: page.summary,
      content: page.content,
      title: page.title,
      date: page.date,
      tags: page.tags,
      image: page.image,
    },
  }
}

export async function getStaticPaths() {
  const posts = getAllSlugPages(['slug'], '_blog')
  const paths = posts.map(({ slug }) => ({
    params: {
      slug,
    },
  }))
  return {
    paths,
    fallback: false,
  }
}

export default function Blog({ title, date, tags, slug, image, content, summary }) {
  return (
    <>
      <SEO
        title={`${title} | Mvochoa`}
        path={`/blog/${slug}`}
        image={image}
        description={summary}
        type="article"
        date={moment(date).format('YYYY-MM-DD')}
      />
      <Layout className="container mx-auto px-4 lg:px-48">
        <article>
          <div className="mb-3">
            <img className="rounded w-full" src={image} alt={title} />
          </div>
          <div className="px-2">
            <div className="font-medium text-sm text-gray-400 mb-3">
              <p className="mb-1">{moment(date).format('LL')}</p>
              <p className="italic font-normal">
                {`${tags}`
                  .replace(/\s*\,\s*/i, ',')
                  .split(',')
                  .join(', ')}
              </p>
            </div>
            <h1 className="font-semibold text-2xl mb-4 sm:text-4xl">{title}</h1>
            <Markdown>{content}</Markdown>
          </div>
          <Adsense slot="3040954327" />
        </article>
      </Layout>
    </>
  )
}
