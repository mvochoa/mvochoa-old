import Layout from '@/components/Layout'
import ListPosts from '@/components/ListPosts'
import Pagination from '@/components/Pagination'
import SEO from '@/components/SEO'
import Adsense from '@/components/Adsense'

import { getAllSlugPages } from '@/lib/markdown'
import { sortByKey } from '@/lib/utils'

export const POST_PER_PAGE = 12
export function calcTotalItemPerPage(items) {
  const pages = parseInt(items.length / POST_PER_PAGE)
  if (items.length % POST_PER_PAGE !== 0) return pages + 1
  return pages
}

export async function getStaticProps({ params }) {
  const page = parseInt(params.page)
  const posts = getAllSlugPages(['slug', 'title', 'summary', 'image', 'date'], '_blog').sort(
    sortByKey('date', -1)
  )
  const indexPage = page * POST_PER_PAGE

  return {
    props: {
      page,
      totalPages: calcTotalItemPerPage(posts),
      posts: posts.slice(indexPage - POST_PER_PAGE, indexPage),
    },
  }
}

export async function getStaticPaths() {
  const posts = getAllSlugPages([], '_blog')
  const paths = []

  for (let page = calcTotalItemPerPage(posts); page > 0; page--) {
    paths.push({ params: { page: `${page}` } })
  }

  return {
    paths,
    fallback: false,
  }
}

export default function Blog({ page, posts = [], totalPages }) {
  return (
    <>
      <SEO title="Blog | Mvochoa" path={`/blog/page/${page}`} />
      <Layout className="container mx-auto px-8">
        <h1 className="py-2 font-semibold text-2xl">Todas las publicaciones</h1>
        <ListPosts posts={posts} />
        <Pagination page={page} totalPages={totalPages} />
        <Adsense slot="2532869629" />
      </Layout>
    </>
  )
}
