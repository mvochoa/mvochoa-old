import moment from 'moment'

import Layout from '@/components/Layout'
import ListPosts from '@/components/ListPosts'
import Pagination from '@/components/Pagination'
import Link from '@/components/Link'
import SEO from '@/components/SEO'

import { getAllSlugPages } from '@/lib/markdown'
import { sortByKey } from '@/lib/utils'

import { calcTotalItemPerPage, POST_PER_PAGE } from './page/[page]'

export async function getStaticProps() {
  const posts = getAllSlugPages(['slug', 'title', 'summary', 'image', 'date'], '_blog').sort(
    sortByKey('date', -1)
  )

  return {
    props: {
      page: 1,
      lastPost: posts[0],
      totalPages: calcTotalItemPerPage(posts),
      posts: posts.slice(1, POST_PER_PAGE),
    },
  }
}

export default function IndexPage({ lastPost, posts = [], page = 1, totalPages }) {
  return (
    <>
      <SEO title="Blog | Mvochoa" path="/blog" />
      <Layout className="container mx-auto px-8">
        <h1 className="py-2 font-semibold text-2xl">Ultima publicación</h1>
        <Link href={`/blog/${lastPost.slug}`} className="block cursor-pointer w-full mb-4">
          <div className="flex justify-between items-start flex-wrap sm:flex-nowrap">
            <div
              style={{ backgroundImage: `url(${lastPost.image})` }}
              className="w-full h-52 bg-no-repeat bg-top bg-cover rounded-t-lg sm:rounded-l-lg sm:rounded-t-none"
            />
            <div className="sm:h-52 sm:overflow-hidden border-b border-r border-l sm:border-t sm:border-l-0 border-gray-200 rounded-b-lg sm:rounded-t-lg sm:rounded-b-lg sm:rounded-l-none p-2 ">
              <p className="text-xs text-gray-600 font-extralight">
                {moment(lastPost.date).format('LL')}
              </p>
              <p className="text-lg font-semibold line-clamp-2">{lastPost.title}</p>
              <p className="text-sm text-gray-800 font-light text-justify line-clamp-3">
                {lastPost.summary}
              </p>
            </div>
          </div>
        </Link>
        <h2 className="py-2 font-semibold text-2xl">Todas las publicaciones</h2>
        <ListPosts posts={posts} />
        <Pagination page={page} totalPages={totalPages} />
      </Layout>
    </>
  )
}
