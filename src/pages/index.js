import Layout from '@/components/Layout'
import CardPost from '@/components/CardPost'
import Button from '@/components/Button'
import SEO from '@/components/SEO'

import { getAllSlugPages } from '@/lib/markdown'
import { sortByKey } from '@/lib/utils'

export async function getStaticProps({ params }) {
  const posts = getAllSlugPages(['slug', 'title', 'summary', 'image', 'date'], '_blog')
    .sort(sortByKey('date', -1))
    .slice(0, 6)
  return {
    props: {
      posts,
    },
  }
}

export default function IndexPage({ posts }) {
  return (
    <>
      <SEO title="Mvochoa" />
      <Layout className="container mx-auto px-8">
        <h2 className="py-2 font-semibold text-2xl">Ultimas publicaciones</h2>
        <div className="flex justify-between items-start flex-wrap">
          {posts.map((post, index) => (
            <CardPost
              key={index}
              to={`/blog/${post.slug}`}
              image={post.image}
              title={post.title}
              summary={post.summary}
              date={post.date}
            />
          ))}
        </div>
        <div className="flex justify-center items-center my-2">
          <Button href="/blog">Ver mas publicaciones</Button>
        </div>
      </Layout>
    </>
  )
}
