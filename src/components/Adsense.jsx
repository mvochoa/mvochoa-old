import Head from 'next/head'

export default function Adsense({ slot }) {
  return (
    <>
      <ins
        suppressHydrationWarning
        className="adsbygoogle"
        style={{ display: 'block' }}
        data-ad-client="ca-pub-9994001811140398"
        data-ad-slot={slot}
        data-ad-format="auto"
        data-full-width-responsive="true"
      ></ins>
      <Head>
        <script
          async
          src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9994001811140398"
          crossOrigin="anonymous"
        ></script>
        <script
          dangerouslySetInnerHTML={{
            __html: `(adsbygoogle = window.adsbygoogle || []).push({});`,
          }}
        />
      </Head>
    </>
  )
}
