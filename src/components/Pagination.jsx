import { useState, useEffect } from 'react'

import Button from '@/components/Button'

export default function Pagination({ page, totalPages, siblingCount = 1 }) {
  const DOTS = '...'
  const [pages, setPages] = useState([])
  const [currentPage, setCurrentPage] = useState(page)

  useEffect(() => {
    const totalPageNumbers = siblingCount + 5

    // Example [1..totalPages]
    if (totalPageNumbers >= totalPages) {
      setPages(range(1, totalPages))
      return
    }

    const leftSiblingIndex = Math.max(currentPage - siblingCount, 1)
    const rightSiblingIndex = Math.min(currentPage + siblingCount, totalPages)

    const shouldShowLeftDots = leftSiblingIndex > 2
    const shouldShowRightDots = rightSiblingIndex < totalPages - 2

    const firstPageIndex = 1
    const lastPageIndex = totalPages

    // Example: [1, 2, 3, ..., totalPages]
    if (!shouldShowLeftDots && shouldShowRightDots) {
      let leftItemCount = 3 + 2 * siblingCount
      let leftRange = range(1, leftItemCount)

      setPages([...leftRange, DOTS, totalPages])
      return
    }

    // Example: [1, ..., 3, 4, totalPages]
    if (shouldShowLeftDots && !shouldShowRightDots) {
      let rightItemCount = 3 + 2 * siblingCount
      let rightRange = range(totalPages - rightItemCount + 1, totalPages)
      setPages([firstPageIndex, DOTS, ...rightRange])
      return
    }

    // Example: [1, ..., 2, 3, 4, ..., totalPages]
    if (shouldShowLeftDots && shouldShowRightDots) {
      let middleRange = range(leftSiblingIndex, rightSiblingIndex)
      setPages([firstPageIndex, DOTS, ...middleRange, DOTS, lastPageIndex])
      return
    }
  }, [currentPage])

  function range(start, end) {
    let length = end - start + 1
    return Array.from({ length }, (_, idx) => idx + start)
  }

  function changePage(index) {
    const move = pages.length - index == 2 ? -1 : 1
    setCurrentPage(pages[index + move])
  }

  return (
    <div className="flex justify-center items-center flex-wrap my-2">
      <Button
        href={page > 1 ? `/blog/page/${page - 1}` : null}
        className="rounded-r-none my-1"
        disabled={page <= 1}
      >
        Anterior
      </Button>
      {pages.map((p, index) => (
        <Button
          key={index}
          href={p !== page && p !== DOTS ? `/blog/page/${p}` : null}
          onClick={p === DOTS ? () => changePage(index) : null}
          className={`${p == page ? ' bg-blue-100' : ''} rounded-t-none rounded-b-none px-2 my-1`}
        >
          {p}
        </Button>
      ))}
      <Button
        href={page < totalPages ? `/blog/page/${page + 1}` : null}
        className="rounded-l-none my-1"
        disabled={page >= totalPages}
      >
        Siguiente
      </Button>
    </div>
  )
}
