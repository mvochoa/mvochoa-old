import Head from 'next/head'

export default function SEO({
  title = '',
  description = 'Artículos, vídeos, recursos y tutoriales sobre programación',
  path = '/',
  image = '/images/logo.png',
  type = 'website',
  date,
}) {
  const domain = 'mvochoa.com'
  const url = `https://${domain}${path}`
  description = `${description}`.slice(0, 155)
  image = `https://${domain}${image}`
  const ld = {
    '@context': 'https://schema.org/',
    '@type': 'NewsArticle',
    headline: title,
    image: [image],
  }
  if (date) {
    ld['datePublished'] = `${date}T00:00:00Z`
  }

  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <link rel="canonical" href={url} />
      <meta property="og:url" content={url} />
      <meta property="og:type" content={type} />
      <meta property="og:locale" content="es_ES" />
      <meta property="og:title" content={title} />
      <meta property="og:image" content={image} />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content="Mvochoa" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:creator" content="@MvochoaMv" />
      <meta name="twitter:domain" content={domain} />
      <meta name="twitter:image" content={image} />
      <meta name="twitter:site" content="@MvochoaMv" />
      {date && <script type="application/ld+json">{JSON.stringify(ld)}</script>}
    </Head>
  )
}
