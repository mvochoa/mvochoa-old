import moment from 'moment'

import Link from '@/components/Link'

export default function CardPost({ title, to, summary, image, date }) {
  return (
    <>
      <Link
        href={to}
        className="flex-auto block cursor-pointer w-full mb-4 lg:mx-2 lg:w-1/4 lg:max-w-md md:w-1/2"
      >
        <div className="m-1 overflow-hidden">
          <div
            style={{ backgroundImage: `url(${image})` }}
            className="w-full h-52 bg-no-repeat bg-top bg-cover rounded-t-lg"
          />
          <div className="h-40 overflow-hidden border border-t-0 border-gray-200 rounded-b-lg p-2 ">
            <p className="text-xs text-gray-600 font-extralight">{moment(date).format('LL')}</p>
            <p className="text-lg font-semibold line-clamp-2">{title}</p>
            <p className="text-sm text-gray-800 font-light text-justify line-clamp-3">{summary}</p>
          </div>
        </div>
      </Link>
    </>
  )
}
