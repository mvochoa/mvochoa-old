import fs from 'fs'

import { getAllSlugPages } from '../src/lib/markdown.mjs'

const getDate = new Date().toISOString()

const DOMAIN = 'https://mvochoa.com/'

const pagesSitemap = [
  ...getAllSlugPages(['slug'], '_blog').map(({ slug }) => `blog/${slug}`),
  ...fs.readdirSync('src/pages').filter((staticPage) => {
    return !['_app.js', '_document.js', '_error.js', 'index.js', 'sitemap.xml.js'].includes(
      staticPage
    )
  }),
]
  .map((staticPagePath) => `${DOMAIN}${staticPagePath}/`)
  .map(
    (url) => `   <url>
    <loc>${url}</loc>
    <lastmod>${new Date().toISOString()}</lastmod>
    <changefreq>monthly</changefreq>
    <priority>1.0</priority>
</url>
`
  )
  .join('')

const generatedSitemap = `<?xml version="1.0" encoding="UTF-8"?>
<urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
>
  <url>
    <loc>${DOMAIN}</loc>
    <lastmod>${new Date().toISOString()}</lastmod>
    <changefreq>monthly</changefreq>
    <priority>1.0</priority>
  </url>
${pagesSitemap}</urlset>
`

fs.writeFileSync('./public/sitemap.xml', generatedSitemap, 'utf8')
