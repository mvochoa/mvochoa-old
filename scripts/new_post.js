const fs = require('fs')
const path = require('path')

let nameFile = null

const args = process.argv.slice(2)
for (let index in args) {
  switch (args[index]) {
    case '--name':
      nameFile = args[parseInt(index) + 1]
      break
    case '--help':
      console.log(`
Usage: node new_post.js [OPTIONS]

Options:

   --name        Name for the new post
        `)
      process.exit(0)
  }
}

function printError(message) {
  console.error(`Error: ${message}`)
  process.exit(1)
}

if (!nameFile) {
  printError('Name for the new post is required')
}

const PATH_BLOG = path.join(process.cwd(), '_blog')
const PATH_BLOG_ASSETS = path.join(process.cwd(), 'public/images/blog')
if (!fs.existsSync(PATH_BLOG)) printError(`Directory does not exist ${PATH_BLOG}`)
if (!fs.existsSync(PATH_BLOG_ASSETS)) printError(`Directory does not exist ${PATH_BLOG_ASSETS}`)

const NOW = new Date()
const TIME = `${NOW.getTime()}`
const SLUG_POST = nameFile
  .toLowerCase()
  .replaceAll(/[\|_\.,\-\+]/g, '')
  .replaceAll(/\s{1,}/g, '-')
  .normalize('NFD')
  .replace(/[\u0300-\u036f]/g, '')
const PATH_POST = path.join(PATH_BLOG, `${TIME}-${SLUG_POST}.md`)
const PATH_POST_ASSETS = path.join(PATH_BLOG_ASSETS, TIME)

fs.writeFileSync(
  PATH_POST,
  `---
slug: '${SLUG_POST}'
title: '${nameFile}'
summary: ''
date: '${NOW.getUTCFullYear()}-${
    NOW.getMonth() < 9 ? `0${NOW.getMonth() + 1}` : NOW.getMonth() + 1
  }-${NOW.getDate() < 10 ? `0${NOW.getDate()}` : NOW.getDate()}'
tags: ''
image: '/images/blog/${TIME}/thumbnail.png'
---

`
)
console.log('Post created:', PATH_POST)

if (!fs.existsSync(PATH_POST_ASSETS)) {
  fs.mkdirSync(PATH_POST_ASSETS, 0755)
  console.log('Directory created:', PATH_POST_ASSETS)
}
